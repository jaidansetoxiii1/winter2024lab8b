import java.util.Scanner;

public class BoardGameApp {
    public static void main(String[] args) {
        java.util.Scanner reader = new java.util.Scanner(System.in);

		
		//MAIN GAME
		Board theBoard = new Board();
		
		int numCastles = 5;
		int turns = 0;
		
		System.out.println("WELCOME TO [CRASH WITH CASTLES]?");
		
		
		//Loops whilst the two variables numCastles and turns are above / below a threshold respectively.
		while (numCastles > 0 && turns < 8) {
			
			//Visual representation of board for the player.
			System.out.println(theBoard.toString());
			System.out.println("Castles Left: " + numCastles);
			System.out.println("Turns Remaining: " + turns);
			
			//Player picks a row / column, then the placeToken instance variable of the board is called with theBoard
			//with the two inputs as parameters.
			System.out.println("Choose a space to mark (Row - Column): ");
			
			int row = reader.nextInt();
			int column = reader.nextInt();
			
			int resultOfGuess = theBoard.placeToken(row,column);
			
			
			//If/Else blocks to determine the outcome of what the player input.
			if (resultOfGuess == -2) {
				System.out.println("\nINVALID - Must be between 1 and " + theBoard.getSIZE());
			}
			else if (resultOfGuess == -1) {
				System.out.println("\nYou've already been there. Try somewhere else.");
			}
			else if (resultOfGuess == 1){
				System.out.println("\nNOT GOOD, YOU HIT A WALL. Insurance is not going to like this.");
				turns++;
			}
			else if (resultOfGuess == 0) {
				System.out.println("\nSUCCESS!! You're 1 castle closer to domination.");
				numCastles--;
				turns++;
			}
			
			
		}
		
		System.out.println(theBoard.toString());
		
		//Checks whether or not the player has placed down all their castle.
		if (numCastles <= 0) {
			//If so, you win.
			System.out.println("YOU'VE WON AND CONQUERED THESE LANDS");
		} 
		else {
			//else, you LOSE.
			System.out.println("OH DEAR, YOUR AMBITIONS HAVE BEEN THWARTED BY WALLS");
		}
    }

}
