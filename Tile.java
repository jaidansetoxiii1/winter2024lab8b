//Enum is the representation of the different states a tile can be in.
public enum Tile {
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("_"),
	CASTLE("C");
	
	private String name;

	private Tile(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}