import java.util.Scanner;
import java.util.Random;

public class Board {
	
	private Tile[][] grid;
	private final int SIZE = 5;
	
	public Board() {
		Random rand = new Random();
		
		//creates the board with the dimensions of the SIZE const
		this.grid = new Tile[SIZE][SIZE];
		
		//Fills the board with blank spaces.
		for (int i = 0; i < SIZE; i++) { 
			
			for (int j = 0; j < SIZE; j++) {
				this.grid[i][j] = Tile.BLANK;
			}
		}
		
		//Fills random spaces with hidden walls, once per row currently
		for (int i = 0; i < SIZE; i++) {
			
			int randIndex = rand.nextInt(SIZE);
			this.grid[i][randIndex] = Tile.HIDDEN_WALL;
			
		}
	}
	
	//Returns a string of board in a readable format for the player.
	public String toString() {
		String board = "  ";
		
		//This for loop creates the top grid guides
		for (int i = 0; i < SIZE; i++) {
			board = board + (i + 1) + " ";
		}
		
		board = board + "\n";
		
		//This prints the contents of the board and the side guides
		for (int i = 0; i < SIZE; i++) { 
			
			board = board + (i + 1)+ " ";
			
			for (int j = 0; j < SIZE; j++) {
				board = board + grid[i][j].getName() + " ";
			}
			
			board = board + "\n";
			
		}
		
		return board;
	}
	
	
	//Places token on the grid. Position is taken as two parameters of row and column.
	public int placeToken(int row, int col) {
		
		//Doing this because my grid goes from 1 - 5 rather than 0 - 4
		row--;
		col--;
		
		//Returns result depending on what is contained in the chosen coordinates of the grid.
		if (row < 0 || row >= SIZE || col < 0 || col >= SIZE){
			return -2;
		}	
		
		else if (this.grid[row][col] == Tile.WALL || this.grid[row][col] == Tile.CASTLE) {
			return -1;
		}	
		
		else if (this.grid[row][col] == Tile.HIDDEN_WALL){
			this.grid[row][col] = Tile.WALL;
			return 1;
		}	
		else {
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
	
	
	//This is just here so I can tell the player the range that they can place their token on
	//if they input an integer that is out of bounds
	public int getSIZE() {
		return this.SIZE;
	}
	
}